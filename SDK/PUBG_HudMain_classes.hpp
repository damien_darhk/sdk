#pragma once

// PlayerUnknown's Battlegrounds (3.6.10.1) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x10)
#endif

#include "PUBG_HudMain_structs.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// WidgetBlueprintGeneratedClass HudMain.HudMain_C
// 0x00B4 (0x03BC - 0x0308)
class UHudMain_C : public UHudMainBaseWidget
{
public:
	struct FPointerToUberGraphFrame                    UberGraphFrame;                                           // 0x0308(0x0008) (Transient, DuplicateTransient)
	class UCanvasPanel*                                HideOnObserverSpectating;                                 // 0x0310(0x0008) (BlueprintVisible, ExportObject, ZeroConstructor, InstancedReference, IsPlainOldData, RepSkip, RepNotify, Interp, NonTransactional, EditorOnly, NoDestructor, AutoWeak, ContainsInstancedReference, AssetRegistrySearchable, SimpleDisplay, AdvancedDisplay, Protected, BlueprintCallable, BlueprintAuthorityOnly, TextExportTransient, NonPIEDuplicateTransient, ExposeOnSpawn, PersistentInstance, UObjectWrapper, HasGetValueTypeHash, NativeAccessSpecifierPublic, NativeAccessSpecifierProtected, NativeAccessSpecifierPrivate)
	struct FScriptMulticastDelegate                    ButtonClickedDispatcher;                                  // 0x0318(0x0010) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, BlueprintAssignable)
	class UHitNotifyWidget_C*                          HitNotify;                                                // 0x0328(0x0008) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	struct FColorBlindColorSet                         ColorBlindColorSet_SpetatingName;                         // 0x0330(0x0010) (Edit, BlueprintVisible, DisableEditOnInstance)
	struct FTimerHandle                                ReplayGetTimeHandler;                                     // 0x0340(0x0008) (Edit, BlueprintVisible, DisableEditOnInstance)
	struct FWidgetTransform                            ObserverSpectatingDownPos;                                // 0x0348(0x001C) (Edit, BlueprintVisible, DisableEditOnInstance)
	struct FWidgetTransform                            ObserverSpectatingUpPos;                                  // 0x0364(0x001C) (Edit, BlueprintVisible, DisableEditOnInstance)
	float                                              LastOptionToggleTime;                                     // 0x0380(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)
	unsigned char                                      UnknownData00[0x4];                                       // 0x0384(0x0004) MISSED OFFSET
	struct FHudUiConfig                                BattleList_Config;                                        // 0x0388(0x0030) (Edit, BlueprintVisible, DisableEditOnInstance)
	float                                              SholderPressedTime;                                       // 0x03B8(0x0004) (Edit, BlueprintVisible, ZeroConstructor, DisableEditOnInstance, IsPlainOldData)

	static UClass* StaticClass()
	{
		static UClass* ptr = nullptr;
		if (!ptr) ptr = UObject::FindClass(0x297ff0a1);
		return ptr;
	}


	void OnKey_EmoteWheelReleased();
	void OnKey_EmoteWheelPressed();
	bool OnInit_Delegate(class ATslBaseHUD** TslBaseHUD);
	bool OnInit_Replay(class ATslBaseHUD** TslBaseHUD);
	bool OnInit_Input(class ATslBaseHUD** TslBaseHUD);
	bool OnInit_Widget(class ATslBaseHUD** TslBaseHUD);
	struct FEventReply OnMouseMove(struct FGeometry* MyGeometry, struct FPointerEvent* MouseEvent);
	void SetObserverSpectatingUp();
	void SetObserverSpectatingDown();
	void ShouldShowReplayMenu(bool* bShow);
	void BindEventForShowReplayTimeline();
	void ShowReplayTimeLine(bool bShow);
	void SetHUDForIngameReplayMenu();
	void BindEventForMapClosing();
	void OnKey_ReplayMenuOrEscape();
	void InitForReplay();
	void OnToggleOption();
	void UpdateReplayTimeline();
	void OnToggleBattleList();
	void OnMapHide();
	void OnMapShow();
	void OnKey_MapReleased();
	void OnKey_MapPressed();
	void OnShowCarePackageItemList();
	void IsShowMapOrInventory(bool* bIsShow);
	void OnTogglePlayerList();
	void IsCharacterAlive(bool* IsAlive);
	void OnNitifyHit(float DamagePercent, EDamageTypeCategory DamageTypeCategory);
	void OnKey_SystemMenuOrEscape();
	void OnToggleMap();
	void OnPossessPawnChange();
	void CreateCheckReplayTimer();
	void CheckReplayTimer();
	void HideMapForReplay();
	void Construct();
	void ExecuteUbergraph_HudMain(int EntryPoint);
	void ButtonClickedDispatcher__DelegateSignature();
};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
