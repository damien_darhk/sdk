#pragma once

// PlayerUnknown's Battlegrounds (3.6.10.1) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x10)
#endif

#include "PUBG_Basic.hpp"
#include "PUBG_InputHookingWidget_classes.hpp"
#include "PUBG_Engine_classes.hpp"
#include "PUBG_UMG_classes.hpp"
#include "PUBG_CoreUObject_classes.hpp"
#include "PUBG_SlateCore_classes.hpp"

namespace Classes
{
}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
