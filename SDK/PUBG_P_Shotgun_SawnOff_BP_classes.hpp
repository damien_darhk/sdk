#pragma once

// PlayerUnknown's Battlegrounds (3.6.10.1) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x10)
#endif

#include "PUBG_P_Shotgun_SawnOff_BP_structs.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// BlueprintGeneratedClass P_Shotgun_SawnOff_BP.P_Shotgun_SawnOff_BP_C
// 0x0000 (0x0460 - 0x0460)
class AP_Shotgun_SawnOff_BP_C : public ATslParticle
{
public:

	static UClass* StaticClass()
	{
		static UClass* ptr = nullptr;
		if (!ptr) ptr = UObject::FindClass(0x82081193);
		return ptr;
	}

};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
