#pragma once

// PlayerUnknown's Battlegrounds (3.6.10.1) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x10)
#endif

#include "PUBG_TslLobby_cus_classes.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function TslLobby_cus.TslLobby_cus_C.ReceiveBeginPlay
struct ATslLobby_cus_C_ReceiveBeginPlay_Params
{
};

// Function TslLobby_cus.TslLobby_cus_C.ExecuteUbergraph_TslLobby_cus
struct ATslLobby_cus_C_ExecuteUbergraph_TslLobby_cus_Params
{
	int                                                EntryPoint;                                               // (Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
