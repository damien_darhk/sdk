#pragma once

// PlayerUnknown's Battlegrounds (3.6.10.1) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x10)
#endif

#include "PUBG_WeapKar98k_classes.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function WeapKar98k.WeapKar98k_C.UserConstructionScript
struct AWeapKar98k_C_UserConstructionScript_Params
{
};

// Function WeapKar98k.WeapKar98k_C.ReceiveBeginPlay
struct AWeapKar98k_C_ReceiveBeginPlay_Params
{
};

// Function WeapKar98k.WeapKar98k_C.ExecuteUbergraph_WeapKar98k
struct AWeapKar98k_C_ExecuteUbergraph_WeapKar98k_Params
{
	int                                                EntryPoint;                                               // (Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
