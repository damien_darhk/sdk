// PlayerUnknown's Battlegrounds (3.6.10.1) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x10)
#endif

#include "PUBG_BP_FppWeaponSlotWidget_parameters.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_FppWeaponSlotWidget.BP_FppWeaponSlotWidget_C.HIddenEnd__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UBP_FppWeaponSlotWidget_C::HIddenEnd__DelegateSignature()
{
	static UFunction* fn = nullptr; 
	 if (!fn) fn = UObject::GetObjectCasted<UFunction>(66598);

	UBP_FppWeaponSlotWidget_C_HIddenEnd__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_FppWeaponSlotWidget.BP_FppWeaponSlotWidget_C.HIddenStart__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UBP_FppWeaponSlotWidget_C::HIddenStart__DelegateSignature()
{
	static UFunction* fn = nullptr; 
	 if (!fn) fn = UObject::GetObjectCasted<UFunction>(66597);

	UBP_FppWeaponSlotWidget_C_HIddenStart__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_FppWeaponSlotWidget.BP_FppWeaponSlotWidget_C.ShowEnd__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UBP_FppWeaponSlotWidget_C::ShowEnd__DelegateSignature()
{
	static UFunction* fn = nullptr; 
	 if (!fn) fn = UObject::GetObjectCasted<UFunction>(66596);

	UBP_FppWeaponSlotWidget_C_ShowEnd__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_FppWeaponSlotWidget.BP_FppWeaponSlotWidget_C.ShowStart__DelegateSignature
// (Public, Delegate, BlueprintCallable, BlueprintEvent)

void UBP_FppWeaponSlotWidget_C::ShowStart__DelegateSignature()
{
	static UFunction* fn = nullptr; 
	 if (!fn) fn = UObject::GetObjectCasted<UFunction>(66595);

	UBP_FppWeaponSlotWidget_C_ShowStart__DelegateSignature_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
