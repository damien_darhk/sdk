#pragma once

// PlayerUnknown's Battlegrounds (3.6.10.1) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x10)
#endif

#include "PUBG_TslLobby_ver_3_classes.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function TslLobby_ver_3.TslLobby_ver_C.ReceiveBeginPlay
struct ATslLobby_ver_C_ReceiveBeginPlay_Params
{
};

// Function TslLobby_ver_3.TslLobby_ver_C.ExecuteUbergraph_TslLobby_ver_3
struct ATslLobby_ver_C_ExecuteUbergraph_TslLobby_ver_3_Params
{
	int                                                EntryPoint;                                               // (Parm, ZeroConstructor, IsPlainOldData)
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
