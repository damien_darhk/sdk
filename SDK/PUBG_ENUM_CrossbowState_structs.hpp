#pragma once

// PlayerUnknown's Battlegrounds (3.6.10.1) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x10)
#endif

#include "PUBG_Basic.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Enums
//---------------------------------------------------------------------------

// UserDefinedEnum ENUM_CrossbowState.ENUM_CrossbowState
enum class ENUM_CrossbowState : uint8_t
{
	ENUM_CrossbowState__NewEnumerator0 = 0,
	ENUM_CrossbowState__NewEnumerator1 = 1,
	ENUM_CrossbowState__NewEnumerator2 = 2,
	ENUM_CrossbowState__ENUM_MAX   = 3
};



}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
