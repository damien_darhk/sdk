// PlayerUnknown's Battlegrounds (3.6.10.1) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x10)
#endif

#include "PUBG_BP_FppWeaponSlotHudWidget_parameters.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Functions
//---------------------------------------------------------------------------

// Function BP_FppWeaponSlotHudWidget.BP_FppWeaponSlotHudWidget_C.FppWeaponReplayTimelineDown
// (Public, BlueprintCallable, BlueprintEvent)

void UBP_FppWeaponSlotHudWidget_C::FppWeaponReplayTimelineDown()
{
	static UFunction* fn = nullptr; 
	 if (!fn) fn = UObject::GetObjectCasted<UFunction>(67182);

	UBP_FppWeaponSlotHudWidget_C_FppWeaponReplayTimelineDown_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_FppWeaponSlotHudWidget.BP_FppWeaponSlotHudWidget_C.FppWeaponReplayTimelineUp
// (Public, BlueprintCallable, BlueprintEvent)

void UBP_FppWeaponSlotHudWidget_C::FppWeaponReplayTimelineUp()
{
	static UFunction* fn = nullptr; 
	 if (!fn) fn = UObject::GetObjectCasted<UFunction>(67177);

	UBP_FppWeaponSlotHudWidget_C_FppWeaponReplayTimelineUp_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_FppWeaponSlotHudWidget.BP_FppWeaponSlotHudWidget_C.Construct
// (BlueprintCosmetic, Event, Public, BlueprintEvent)

void UBP_FppWeaponSlotHudWidget_C::Construct()
{
	static UFunction* fn = nullptr; 
	 if (!fn) fn = UObject::GetObjectCasted<UFunction>(67176);

	UBP_FppWeaponSlotHudWidget_C_Construct_Params params;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


// Function BP_FppWeaponSlotHudWidget.BP_FppWeaponSlotHudWidget_C.ExecuteUbergraph_BP_FppWeaponSlotHudWidget
// (HasDefaults)
// Parameters:
// int                            EntryPoint                     (Parm, ZeroConstructor, IsPlainOldData)

void UBP_FppWeaponSlotHudWidget_C::ExecuteUbergraph_BP_FppWeaponSlotHudWidget(int EntryPoint)
{
	static UFunction* fn = nullptr; 
	 if (!fn) fn = UObject::GetObjectCasted<UFunction>(67154);

	UBP_FppWeaponSlotHudWidget_C_ExecuteUbergraph_BP_FppWeaponSlotHudWidget_Params params;
	params.EntryPoint = EntryPoint;

	auto flags = fn->FunctionFlags;

	UObject::ProcessEvent(fn, &params);

	fn->FunctionFlags = flags;
}


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
