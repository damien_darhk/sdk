#pragma once

// PlayerUnknown's Battlegrounds (3.6.10.1) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x10)
#endif

#include "PUBG_P_Environment_Leaf_BP_Lobby_classes.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Parameters
//---------------------------------------------------------------------------

// Function P_Environment_Leaf_BP_Lobby.P_Environment_Leaf_BP_Lobby_C.UserConstructionScript
struct AP_Environment_Leaf_BP_Lobby_C_UserConstructionScript_Params
{
};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
