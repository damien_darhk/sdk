#pragma once

// PlayerUnknown's Battlegrounds (3.6.10.1) SDK

#ifdef _MSC_VER
	#pragma pack(push, 0x10)
#endif

#include "PUBG_OnlineSubsystemSteam_structs.hpp"

namespace Classes
{
//---------------------------------------------------------------------------
//Classes
//---------------------------------------------------------------------------

// Class OnlineSubsystemSteam.SteamNetConnection
// 0x0000 (0x65690 - 0x65690)
class USteamNetConnection : public UIpConnection
{
public:

  __forceinline bool& bIsPassthrough()
      {
        return reinterpret_cast<bool&>(*reinterpret_cast<bool*>(reinterpret_cast<uintptr_t>(this) + 0x65688));
      }

	static UClass* StaticClass()
	{
		static UClass* ptr = nullptr;
		if (!ptr) ptr = UObject::FindClass(0x69453104);
		return ptr;
	}

};


// Class OnlineSubsystemSteam.SteamNetDriver
// 0x0010 (0x0490 - 0x0480)
class USteamNetDriver : public UIpNetDriver
{
public:
	unsigned char                                      UnknownData00[0x10];                                      // 0x0480(0x0010) MISSED OFFSET

	static UClass* StaticClass()
	{
		static UClass* ptr = nullptr;
		if (!ptr) ptr = UObject::FindClass(0x7fd5b446);
		return ptr;
	}

};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
